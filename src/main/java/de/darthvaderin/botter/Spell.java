
package de.darthvaderin.botter;

public class Spell {
    private String name;
    private String vglname;
    /*message 1 ist fues mit Namen nach Zauberspruch, 1a ist vor Namen, 1b nach Namen, message 2 ist fuer reinen Zauberspruch*/
    private String message1a; 
    private String message1b;
    private String message2;
    private boolean zaubern;
    private boolean zwei;

    public Spell(String name, String message1a, String message1b, String message2, boolean zaubern) {
        this.name = name;
        this.vglname = name;
        this.message1a = message1a;
        this.message1b = message1b;
        this.message2 = message2;
        this.zaubern = zaubern;
        this.zwei=false;
    }

    public String getName() {
        return name;
    }

    public String getMessage1a() {
        return message1a;
    }

    public String getMessage1b() {
        return message1b;
    }

    public String getMessage2() {
        return message2;
    }

    public boolean isZaubern() {
        return zaubern;
    }

    public String getVglname() {
        return vglname;
    }

    public void setVglname(String vglname) {
        this.vglname = vglname;
        zwei = true;
    }

    public boolean isZwei() {
        return zwei;
    }
    
}
