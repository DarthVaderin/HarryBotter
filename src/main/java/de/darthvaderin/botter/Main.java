
package de.darthvaderin.botter;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.util.DiscordException;

public class Main {
   public static void main(String[] args) throws DiscordException {
        String token = "Token hier einfuegen";
        IDiscordClient client = Main.getClient(token, true); // Gets the client object (from the first example)
        EventDispatcher dispatcher = client.getDispatcher(); // Gets the EventDispatcher instance for this client instance
        dispatcher.registerListener(new AnnotationListener()); // Registers the @EventSubscriber example class from above
  }
     
     public static IDiscordClient getClient(String token, boolean login) throws DiscordException { // Returns an instance of the Discord client
        ClientBuilder clientBuilder = new ClientBuilder(); // Creates the ClientBuilder instance
        clientBuilder.withToken(token); // Adds the login info to the builder
        
        if (login) {
            return clientBuilder.login(); // Creates the client instance and logs the client in
        } else {
            return clientBuilder.build(); // Creates the client instance but it doesn't log the client in yet, you would have to call client.login() yourself
        }
    }
     /*Für Registrierung in https://discordapp.com/developers/applications/me eine neue Applikation hinzufuegen, speichern, 
     Bot User createn, dessen Token hier oben einfuegen, den Bot in euren Entwicklungsumgebung starten und mit 
     https://discordapp.com/oauth2/authorize?&client_id=EURECLIENTID&scope=bot 
     zu eurem Server hinzufügen*/
}
