
package de.darthvaderin.botter;

public class Haus {
    private String name;
    private int punkte;
    private String common_room; /*versteckter Gemeinschaftsraum, wo man nur mit Passwort reinkommen soll*/
    private String key; /*Damit sollte die mit Passwoertern beschraenkte Zugriff auf die Gemeinschaftsraeme geloest werden, ist aber auch nicht fertig, da keine Zeit*/
    private String eingangsname; /*channel name von Raum wo Passwort eingegeben werden sollte (Raum vor der EIngangstuer)*/

    public Haus(String name, int punkte, String common_room, String key, String eingangsname) {
        this.name = name;
        this.punkte = punkte;
        this.common_room = common_room;
        this.key = key;
        this.eingangsname = eingangsname;
    }
    
    public Haus(String name){
        this.name = name;
        this.punkte = 0;
        fill(name);
        
    }

    public String getName() {
        return name;
    }

    public int getPunkte() {
        return punkte;
    }

    public String getCommon_room() {
        return common_room;
    }

    public String getKey() {
        return key;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPunkte(int punkte) {
        this.punkte = punkte;
    }

    public void setCommon_room(String common_room) {
        this.common_room = common_room;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
    public void addPunkte(int punkte){
        this.punkte+=punkte;
    }

    public String getEingangsname() {
        return eingangsname;
    }
    public void fill(String name){
        if(name!=null){
        if(name.equalsIgnoreCase("Gryffindor")){
            this.common_room = "Gg";
            this.key = "Warzenschwein";
            this.eingangsname = "portraet-fette-dame";
        } else if(name.equalsIgnoreCase("Ravenclaw")){
            this.common_room = "Rg";
            this.key = "Nicht du";
            this.eingangsname = "ravenclaw-turm";
        } else if(name.equalsIgnoreCase("Hufflepuff")){
            this.common_room = "Hg";
            this.key = "Klopf Klopf Pause Klopf-Klopf-Klopf";
            this.eingangsname = "weinkeller";
        } else if(name.equalsIgnoreCase("Slytherin")){
            this.common_room = "Sg";
            this.key = "Reinblüter";
            this.eingangsname = "dungeon";    
        } else{
            this.common_room = "Error";
            this.key = "Error";
            this.eingangsname = "eingangshalle";
        }
        }
        else{
            
        }
    }
}
