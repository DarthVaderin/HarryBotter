package de.darthvaderin.botter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 *
 * @author DarthVaderin
 */
public class CommandsHWServer {
    private List<Haus> haus;
    private List<Spell> spells;
    public CommandsHWServer(){
        haus = new ArrayList<Haus>();
        haus.add(new Haus("Gryffindor"));
        haus.add(new Haus("Ravenclaw"));
        haus.add(new Haus("Hufflepuff"));
        haus.add(new Haus("Slytherin"));
        try {
            Utils.reader(haus);
        } catch (IOException ex) {
            System.out.println("Did not read!");
        }
        spells = zauberspruecheAdden();
        
    }
    
    public void hogwartsPrefix(IMessage message, String []args) throws MissingPermissionsException, RateLimitException, DiscordException{
        /*Zur Abfrage der Hauspunkte*/            
             if(args[0].equalsIgnoreCase("table")||args[0].equalsIgnoreCase("stundenglas")){
                table(message);
                
             } else if (args[0].equalsIgnoreCase("help")) {
                String sp = "";
                sp += help(sp);
                message.getChannel().sendMessage(sp);
            } else if (args[0].equalsIgnoreCase("helpv")) {
                message.getChannel().sendMessage("Du kannst als Vertrauensschüler Punkte über '10 points to Hausname' oder '5 Punkte für Hausname' vergeben und mit '1 Punkte Abzug fÃ¼r Hausname' oder '50 points taken from Hausname' wegnehmen. Gebrauche es aber mit Vorsicht. ");
  
        /*<SORTING COMMAND*/                
            } else if(args[0].equalsIgnoreCase("sorting")){
                    sorting(message, args);
        /*zukünftig, um in Gemeinschaftsräume nur über Passwort Zugang zu erhalten*/                    
            /*} else if(args[0].equalsIgnoreCase("einlass")){
                CommandsHWServer.gemeinschaftseinlass(message, haus, args, null);*/
        /*Für den kommenden Hogwartsserver, um bloed zwischen den Räumen mit Berechtigungen hin und her zu springen. Nope nicht realisiert, als es ein eigener Server war*/                
            /*} else if(args[0].equalsIgnoreCase("Flohpulver")){
                CommandsHWServer.flohpulver(message);*/
    }
    }
    
    public void hogwartsNoFix(IMessage message, String [] argum) throws MissingPermissionsException, RateLimitException, DiscordException, IOException{
        int points = 0; /*Für Punkte, wird aber auch von spells ausgeführt(und kommenden Präfixfreien*/  
                try{
                    points = Integer.parseInt(argum[0]);
                } catch(Exception ex){
                    points=-1;
                }
        List<IRole> roles = message.getAuthor().getRolesForGuild(message.getGuild());
        boolean v = false; /*boolean istVertrauensschueler*/
        for(int i=0;i<roles.size();i++){
            if(roles.get(i).getName().equals("v")){
                v = true;
            }
        }
        /*Vergeben der Punkte, v ist die Rolle der Vertrauensschueler*/
                if(points>=0&v){
                    if(argum[1].equalsIgnoreCase("points")&&argum[2].equalsIgnoreCase("to")||argum[1].equalsIgnoreCase("Punkte")&&argum[2].equalsIgnoreCase("für")){
                        Haus name = Utils.givePoints(points,argum[3],haus);
                        if(name!=null){
                            message.getChannel().sendMessage(name.getName() + " now has " + name.getPunkte() +" points");
                            Utils.writer(haus);
                            System.out.println(message.getAuthor().getName() + ": " + name.getName() + " +" + argum[3]);
                        }
                        else{
                            message.getChannel().sendMessage("Fehlgeschlagen. Versuchs nochmal");
                        }
                    } else if((argum[1].equalsIgnoreCase("Punkte")&&argum[2].equalsIgnoreCase("Abzug")&&argum[3].equalsIgnoreCase("für"))||(argum[1].equalsIgnoreCase("points")&&argum[2].equalsIgnoreCase("taken")&&argum[3].equalsIgnoreCase("from"))) {
                        int x = 0 - points;
                        Haus name = Utils.givePoints(x,argum[4],haus);
                        message.getChannel().sendMessage(name.getName() + " hat jetzt " + name.getPunkte() +" Punkte");
                        Utils.writer(haus);
                        System.out.println(message.getAuthor().getName() + ": " + name.getName() + " +" + argum[4]);
                    }
                } 
                
/*Für Zaubersprüche*/                
                for (int i=0;i<spells.size();i++){
                        if(argum[0].equalsIgnoreCase(spells.get(i).getVglname())){
                            if(spells.get(i).isZaubern()&!spells.get(i).isZwei()){
                                message.getChannel().sendMessage(Utils.zaubern(spells.get(i).getMessage1a(), spells.get(i).getMessage1b(),spells.get(i).getMessage2(), argum));
                            }
                            else if (spells.get(i).isZwei()){ /*Fuer zwei Worter lange Zauberspruche. Haette man besser loesen koennen, war aber zu faul*/
                                message.getChannel().sendMessage(Utils.zaubernZwei(spells.get(i).getMessage1a(), spells.get(i).getMessage1b(),spells.get(i).getMessage2(), argum));
                            } else{
                                message.getChannel().sendMessage(spells.get(i).getMessage2());
                            }
                        }
                    }
    }
    
    
    /*Anzeigen der Punkte der Haeuser*/
    public void table( IMessage message) throws MissingPermissionsException, RateLimitException, DiscordException{
        for(int i=0;i<haus.size();i++){
                    String n = haus.get(i).getName();
                    String p = haus.get(i).getPunkte()+"";
                    message.getChannel().sendMessage(n+": "+p);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        System.out.println("should happen");
                    }
                }
    }
    /*Nicht fertig entwickelt, sollte zum Wechseln zwischen Textchanneln sein, was aber nicht so wirklich geht.*/
    public void flohpulver(IMessage message) throws MissingPermissionsException, RateLimitException, DiscordException{
        if(message.getGuild().getName().equals("Hogwarts")){
                    //flohpulver(message,args[1]);
                }
                else{
                    message.getChannel().sendMessage("Geh nach Hogwarts!");
                }
    }
   /*Einsortieren in Haeuser*/
    public void sorting(IMessage message, String [] args) throws MissingPermissionsException, RateLimitException, DiscordException{
        if(!Utils.sorted(message, haus)){
                    if(args.length>1){
                        for(int i=0;i<haus.size();i++){
                            if(args[1].equalsIgnoreCase(haus.get(i).getName())){
                                IRole r = message.getGuild().getRolesByName(haus.get(i).getName()).get(0);
                                message.getAuthor().addRole(r);
                                message.getChannel().sendMessage("Herzlich Willkommen bei "+haus.get(i).getName());
                            }
                        }
                        if(args[1].equalsIgnoreCase("random")){
                            Random randomGenerator = new Random();
                            int randomInt = randomGenerator.nextInt(haus.size());
                            IRole r = message.getGuild().getRolesByName(haus.get(randomInt).getName()).get(0);
                            message.getAuthor().addRole(r);
                            message.getChannel().sendMessage("Herzlich Willkommen bei "+haus.get(randomInt).getName());
                        }
                        System.out.println("Sorted " + message.getAuthor().getName());
                        
                    } else{
                         message.getChannel().sendMessage("Du musst nach <sorting dein Wunschhaus oder random eingeben");
                         
                    }
                    }else{
                        message.getChannel().sendMessage("Häuser wechseln geht nicht.");
                    }
    }
    /*Damit sollte die mit Passwoertern beschraenkte Zugriff auf die Gemeinschaftsraeme geloest werden, ist aber auch nicht fertig, da keine Zeit*/
    public void gemeinschaftseinlass(IMessage message, String [] args, List<IRole> roles) throws MissingPermissionsException, RateLimitException, DiscordException{
        boolean in = false;
                boolean rrole = false;
                for(int i=0;i<haus.size();i++){
                    if(message.getChannel().getName().equalsIgnoreCase(haus.get(i).getEingangsname())){ //
                        in = true;
                        
                        for(int j=0;j<roles.size();j++){
                            if(roles.get(j).getName().equalsIgnoreCase(haus.get(i).getName())){
                                rrole = true;
                                String x = "";
                                for(int m=1;m<args.length-1;m++){
                                    x+= args[m]+" ";
                                }
                                x+=args[args.length-1];
                                if(x.equalsIgnoreCase(haus.get(i).getKey())){
                                    message.getAuthor().addRole(message.getGuild().getRolesByName(haus.get(i).getCommon_room()).get(0));
                                    message.getChannel().sendMessage("Klick Klick... offen");
                                }
                                else{
                                    message.getChannel().sendMessage("Falsches Passwort. Tipp: "+ haus.get(i).getKey() +". Du hast "+x +" eingegeben");
                                }
                            }
                        }
                    } 
                }
                if(!in){
                    message.getChannel().sendMessage("Falscher Raum");
                }
                else if (!rrole){
                    message.getChannel().sendMessage("Für dich falscher Raum");
                }
    }
    
    public String help(String sp){
        for(int i=0;i<spells.size()-1;i++){
                        sp+=spells.get(i).getName()+", ";
                    }
                    sp+=spells.get(spells.size()-1).getName()+". ";
                     return "Diese Zaubersprüche kannst du bisher zaubern: "+ sp +"\nMit <sorting und dann deinem gewünschten Hausnamen oder random kannst du dich als Erstklässler einsortieren lassen. Mit <table oder <stundenglas erfährt du die Punkte der Häuser. Vertrauensschüler finden bei <helpv Hilfe";
    }
    
    public static List<Spell> zauberspruecheAdden(){
        List<Spell> spells = new ArrayList<>();
        spells.add(new Spell("Accio","Hier ist ",".","Was willst du haben?", true));
        spells.add(new Spell("Amnesia",""," weiß jetzt nichts mehr.","Du hast alles vergessen.", true));
        spells.add(new Spell("Anapneo",""," atmet tief durch","*atmet*", true));
        spells.add(new Spell("Avis","","",":bird: :bird: :bird:", false));
        spells.add(new Spell("Aguamenti","Jetzt ist ","voll mit Wasser. :sweat_drops: ","Platsch! :sweat_drops: ", true));
        spells.add(new Spell("Alohomora","","","Die Tür ist geöffnet", false));
        spells.add(new Spell("Avada Kedavra","","ist soeben verstorben.","Du hast deinen Gegenüber soeben umgebracht.", false));
        spells.get(spells.size()-1).setVglname("Avada");
        spells.add(new Spell("Confringo","","","Boom! :boom:", false));
        spells.add(new Spell("Confundo","Verwirrtes ","","Du verwirrst mich...", true));
        spells.add(new Spell("Crucio","","","Aua", false));
        spells.add(new Spell("Expelliarmus","Whooosch! ","s Zauberstab fliegt auf dich zu.","Whoop. Und ein weiterer Zauberstab gehört dir.", true));
        spells.add(new Spell("Expulso","","","Boom! :boom: ", false));
        spells.add(new Spell("Finite","Die Zauber von ","sind jetzt unwirksam.","Die Zauber haben aufgehört zu wirken.", true));
        spells.add(new Spell("Furunkulus","","ist nun voll mit Furunkeln.","Igitt!", true));
        spells.add(new Spell("Incendio","Du hast ","angezündet.",":fire: ", true));
        spells.add(new Spell("Langlock","Halt die Klappe ","","Wen?", true));
        spells.add(new Spell("Levicorpus","","hängt an den Beinen in der Luft.",":skate_falschrum:", true));
        spells.add(new Spell("Liberacorpus","Beendet das in der Luft hängen von ","","Plumps.", true));
        spells.add(new Spell("Lumos","","","Es werde Licht!", false));
        spells.add(new Spell("Muffliato","","","*Quiiiiiieck*", false));
        spells.add(new Spell("Nox","","","Es werde Dunkelheit!", false));
        spells.add(new Spell("Oppugno","","","https://www.youtube.com/watch?v=Lw0FP9putKM", false));
        spells.add(new Spell("Orchideus","Hier,",":bouquet:",":bouquet:", true));
        spells.add(new Spell("Quietus","","",".", false));
        spells.add(new Spell("Reparo","","wurde repariert.","Es wurde repariert.", true));
        spells.add(new Spell("Rictusempra","Kitzelt ","","Hihihihi", true));
        spells.add(new Spell("Scourgify","Jetzt ist ","wieder trocken. Oder feucht, je nachdem wie gut du bist.","Alles wieder trocken!", true));
        spells.add(new Spell("Sectumsempra","","hat jetzt großes Aua.","Du wagst es den Spruch des Halbblutprinzen zu verwenden??!?", true));
        spells.add(new Spell("Sonorus","**","**","**Hallo**", true));
        spells.add(new Spell("Spuck Schnecken","","spuckt jetzt Schnecken: :thermometer_face: :snail:",":thermometer_face: :snail: ", true));
        spells.get(spells.size()-1).setVglname("Spuck"); /*Weil 2 Woerter langer Zauberspruch*/
        spells.add(new Spell("Tarantallegra","",": :dancer: ",":dancer:", true));
        spells.add(new Spell("Vulnera Sanentur","","wird geheilt.","*heil*", true));
        spells.get(spells.size()-1).setVglname("Vulnera");
        spells.add(new Spell("Wingardium Leviosa","","nimmt jetzt Flugstunden","Hui!", true));
        spells.get(spells.size()-1).setVglname("Wingardium");
        return spells;
    }
}
