
package de.darthvaderin.botter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;

public class Utils {
    /**/
    
    
    public static boolean sorted(IMessage message, List<Haus> haus){
        List <IRole> r = message.getAuthor().getRolesForGuild(message.getGuild());
                    for(int i=0;i<r.size();i++){
                        for(int j=0;j<haus.size();j++){
                            if( r.get(i).getName().equals(haus.get(j).getName())){
                                return true;
                            }
                        }
                       
                    }
        return false;
    }
    /*Ja, ich speichere meine Punkte in txt Files. Verurteil mich halt xD Oder machs besser, aber für die Zwecke tuts es*/
    public static void writer( List<Haus> haus) throws IOException{
        FileWriter fw = new FileWriter("punktestand.txt");
        BufferedWriter bw = new BufferedWriter(fw);
        
        for(int i=0;i<haus.size();i++){
            bw.write(haus.get(i).getName() + ":" + haus.get(i).getPunkte()+"-");
        }
        
        bw.close();
        }
    
    public static void reader( List<Haus> haus) throws FileNotFoundException, IOException{
        FileReader fr = new FileReader("punktestand.txt");
        BufferedReader br = new BufferedReader(fr);
        String zeile1 = br.readLine();
        String [] zeilen = zeile1.split("-");
        for(int i=0;i<zeilen.length;i++){
            String [] hululu = zeilen[i].split(":");
            if(hululu.length>1){
            for(int j=0;j<haus.size();j++){
                if(haus.get(j).getName().equalsIgnoreCase(hululu[0])){
                    try{
                        haus.get(j).setPunkte(Integer.parseInt(hululu[1]));
                    } catch(Exception e){System.out.println("did not parse");}
                    }
                }
            }
            else{
                System.out.println("Problem at hululu");
            }
        }
        System.out.println(zeile1);
        br.close();
    }
    
    /*Praktisch zum Testen*/
    public static void printHaueser( List<Haus> haus){
        System.out.println("Print Häuser");
        for(int i=0;i<haus.size();i++){
            System.out.println(haus.get(i).getName());
            System.out.println(haus.get(i).getKey());
            System.out.println(haus.get(i).getCommon_room());
            System.out.println(haus.get(i).getPunkte()+"");
            System.out.println(haus.get(i).getEingangsname());
        }
    }
    public static String zaubern(String message1a, String message1b, String message2 ,String[]agr){
        if(agr.length>1){
                            String zurep = "";
                            for(int i=1;i<agr.length;i++){
                                zurep+=agr[i]+" ";
                            }
                            return message1a + zurep + message1b;
                        }
                        else{
                            return message2;
                        }
    }
    
    public static String zaubernZwei(String message1a, String message1b, String message2 ,String[]agr){
        if(agr.length>2){
                            String zurep = "";
                            for(int i=2;i<agr.length;i++){
                                zurep+=agr[i]+" ";
                            }
                            return message1a + zurep + message1b;
                        }
                        else{
                            return message2;
                        }
    }
    
    public static Haus givePoints(int points, String house, List<Haus> haus){
                if(house.equalsIgnoreCase("gryffindor")|house.equalsIgnoreCase("gryffindor!")){
                    haus.get(0).addPunkte(points);
                    return haus.get(0);
                }
                else if(house.equalsIgnoreCase("ravenclaw")|house.equalsIgnoreCase("ravenclaw!")){
                    haus.get(1).addPunkte(points);
                    return haus.get(1);
                }
                else if(house.equalsIgnoreCase("hufflepuff")|house.equalsIgnoreCase("hufflepuff!")){
                    haus.get(2).addPunkte(points);
                    return haus.get(2);
                }
                else if(house.equalsIgnoreCase("slytherin")|house.equalsIgnoreCase("slytherin!")){
                    haus.get(3).addPunkte(points);
                    return haus.get(3);
                }
                else{
                    return null;
                }
    }
    
    
}
