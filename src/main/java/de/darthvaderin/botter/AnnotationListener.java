
package de.darthvaderin.botter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.sound.sampled.UnsupportedAudioFileException;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.UserJoinEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class AnnotationListener {
    private String botprefix = "<";
    CommandsHWServer hogwarts;
    

    public AnnotationListener() {
        hogwarts = new CommandsHWServer();
    }

    @EventSubscriber
    // Receive messages
    public void OnMesageEvent(MessageReceivedEvent event) throws IOException, UnsupportedAudioFileException, RateLimitException, MissingPermissionsException, DiscordException {
        IMessage message = event.getMessage(); // Get message from event
        if (message.getContent().startsWith(botprefix)) {
            String command = message.getContent().replaceFirst(botprefix, ""); // Remove prefix
            String[] args = command.split(" "); // Split command into arguments
            
            //if(message.getGuild().getName().equalsIgnoreCase("Hogwarts")){ /*Falls du den Bot zu mehreren Servern connecten willst, aber die Commands nur auf manchen Servern funktionieren sollen.*/
                 hogwarts.hogwartsPrefix(message, args);
            
            /*Ab hier ohne PrÃ¤fix kommandos!*/
        } else {
            String full = message.getContent();
            String[] argum = full.split(" ");
            hogwarts.hogwartsNoFix(message, argum);
        }
    }

    @EventSubscriber 
    public void joiner(UserJoinEvent event) throws MissingPermissionsException, RateLimitException, DiscordException {
        if(event.getGuild().getName().equals("Hogwarts")){
            event.getGuild().getChannelsByName("eingangshalle").get(0).sendMessage("Hallo" + event.getUser().mention() + "Willkommen in Hogwarts! Mit <sorting und dann deinem gewünschten Haus oder random kannst du dich selbst einsortieren :)");
        }
    }
    /*soll fuer den Zugang zu GemeinschaftsrÃ¤umen sein, dass man bei jedem online kommen Passwort neu eingeben muss, aber nicht fertig entwickelt. VIel Spaß damit :)*/
    /*@EventSubscriber
     public void changeStatus(PresenceUpdateEvent event) throws MissingPermissionsException, RateLimitException, DiscordException{
     if(event.getNewPresence() == event.getNewPresence().ONLINE){
     List<IRole> rols = event.getUser().getRolesForGuild(g);
     for(int i=0;i<rols.size();i++){
     for(int j=0;j<haus.size();j++){
     if(rols.get(i).getName().equals(haus.get(j).getCommon_room())){
     event.getUser().removeRole(rols.get(i));
     }   
     }
     }
     }
     }*/
    
    
}
